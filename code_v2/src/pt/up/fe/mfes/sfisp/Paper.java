//
// THIS FILE IS AUTOMATICALLY GENERATED!!
//
// Generated at 2015-12-14 by the VDM++ to JAVA Code Generator
// (v9.0.6 - Fri 27-Mar-2015 22:08:23 +0900)
//
// ***** VDMTOOLS START Name=HeaderComment KEEP=NO
// ***** VDMTOOLS END Name=HeaderComment

// This file was genereted from "D:\\Development\\workspace\\overture\\SfISP\\main\\Paper.vdmpp".

// ***** VDMTOOLS START Name=package KEEP=NO
package pt.up.fe.mfes.sfisp;
// ***** VDMTOOLS END Name=package

// ***** VDMTOOLS START Name=imports KEEP=NO

import java.util.Set;
import java.util.HashSet;
import java.util.Iterator;

import jp.vdmtools.VDM.UTIL;
import jp.vdmtools.VDM.Record;
import jp.vdmtools.VDM.Sentinel;
import jp.vdmtools.VDM.EvaluatePP;
import jp.vdmtools.VDM.CGException;
// ***** VDMTOOLS END Name=imports


public class Paper implements EvaluatePP {

    // ***** VDMTOOLS START Name=title KEEP=NO
    public volatile String title = null;
// ***** VDMTOOLS END Name=title

    // ***** VDMTOOLS START Name=authors KEEP=NO
    public volatile Set authors = new HashSet();
// ***** VDMTOOLS END Name=authors

    // ***** VDMTOOLS START Name=date KEEP=NO
    public volatile Utils.Date date = null;
// ***** VDMTOOLS END Name=date

    // ***** VDMTOOLS START Name=affiliation KEEP=NO
    public volatile String affiliation = null;
// ***** VDMTOOLS END Name=affiliation

    // ***** VDMTOOLS START Name=refs KEEP=NO
    public volatile Set refs = new HashSet();
// ***** VDMTOOLS END Name=refs

    // ***** VDMTOOLS START Name=sentinel KEEP=NO
    volatile PaperSentinel sentinel;
// ***** VDMTOOLS END Name=sentinel


    // ***** VDMTOOLS START Name=PaperSentinel KEEP=NO
    class PaperSentinel extends Sentinel {

        public final int Paper = 0;

        public final int nr_functions = 1;

        public PaperSentinel() throws CGException {
        }

        public PaperSentinel(EvaluatePP instance) throws CGException {
            init(nr_functions, instance);
        }

    }
// ***** VDMTOOLS END Name=PaperSentinel
    ;

    // ***** VDMTOOLS START Name=evaluatePP#1|int KEEP=NO
    public Boolean evaluatePP(int fnr) throws CGException {
        return Boolean.TRUE;
    }
// ***** VDMTOOLS END Name=evaluatePP#1|int

    // ***** VDMTOOLS START Name=setSentinel KEEP=NO
    public void setSentinel() {
        try {
            sentinel = new PaperSentinel(this);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
// ***** VDMTOOLS END Name=setSentinel

    // ***** VDMTOOLS START Name=vdm_init_Paper KEEP=NO
    private void vdm_init_Paper() {
        try {
            setSentinel();
        } catch (Exception e) {
            e.printStackTrace(System.out);
            System.out.println(e.getMessage());
        }
    }
// ***** VDMTOOLS END Name=vdm_init_Paper

    // ***** VDMTOOLS START Name=inv_Paper KEEP=NO
    public Boolean inv_Paper() {
        boolean tmpQuant_4 = false;
        {
            Set tmpSet_13 = new HashSet(refs);
            for (Iterator enm_12 = tmpSet_13.iterator(); enm_12.hasNext() && !tmpQuant_4; ) {
                Paper elem_11 = (Paper) enm_12.next();
                Paper x = null;
        /* x */
                x = elem_11;
                try {
                    if (Utils.isAfterOrEqual((x.date), date).booleanValue())
                        tmpQuant_4 = true;
                } catch (CGException e) {
                    e.printStackTrace();
                }
            }
        }
        return Boolean.valueOf(!tmpQuant_4);
    }
// ***** VDMTOOLS END Name=inv_Paper

    // ***** VDMTOOLS START Name=Paper KEEP=NO
    public Paper() throws CGException {
        vdm_init_Paper();
    }
// ***** VDMTOOLS END Name=Paper

    // ***** VDMTOOLS START Name=Paper#5|String|Set|String|Set KEEP=NO
    public Paper(final String t, final Set a, final Utils.Date d, final String af, final Set rfs) throws CGException {
        vdm_init_Paper();
        if (!this.pre_Paper(t, a, d, af, rfs).booleanValue())
            UTIL.RunTime("Precondition failure in Paper");
        title = UTIL.ConvertToString(UTIL.clone(t));
        authors = (Set) UTIL.clone(a);
        date = (Utils.Date) UTIL.clone(d);
        affiliation = UTIL.ConvertToString(UTIL.clone(af));
        refs = (Set) UTIL.clone(rfs);
    }
// ***** VDMTOOLS END Name=Paper#5|String|Set|String|Set

    // ***** VDMTOOLS START Name=pre_Paper#5|String|Set|String|Set KEEP=NO
    public Boolean pre_Paper(final String t, final Set a, final Utils.Date d, final String af, final Set rfs) throws CGException {
        boolean tmpQuant_8 = false;
        {
            Set tmpSet_17 = new HashSet(rfs);
            for (Iterator enm_16 = tmpSet_17.iterator(); enm_16.hasNext() && !tmpQuant_8; ) {
                Paper elem_15 = (Paper) enm_16.next();
                Paper x = null;
        /* x */
                x = elem_15;
                if (Utils.isAfterOrEqual((x.date), d).booleanValue())
                    tmpQuant_8 = true;
            }
        }
        return Boolean.valueOf(!tmpQuant_8);
    }
// ***** VDMTOOLS END Name=pre_Paper#5|String|Set|String|Set


    @Override
    public boolean equals(Object obj) {
        boolean isEqual = false;

        if (!(obj instanceof Paper))
            return isEqual;

        Paper p = (Paper) obj;
        isEqual = (this.title.compareTo(p.title) == 0) ? true : false;

        return isEqual;
    }
}
;

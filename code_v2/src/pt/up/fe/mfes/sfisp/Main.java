package pt.up.fe.mfes.sfisp;

/**
 * Created by asmen on 14/12/2015.
 */
import jp.vdmtools.VDM.CGException;

import java.util.HashSet;

/**
 * Created by asmen on 13/12/2015.
 */
public class Main {


    public static void main(String[] args) {
        try {
            Author daniel = new Author("Daniel", new HashSet<String>(0));
            Author jose = new Author("Jose", new HashSet<String>(0));
            Author rodolfo = new Author("Rodolfo", new HashSet<String>(0));

            Utils.Date dP1 = new Utils.Date(1, 1, 2001);
            Utils.Date dP2 = new Utils.Date(3, 3, 2013);
            Utils.Date dP3 = new Utils.Date(5, 5, 2015);

            HashSet<Author> aP1 = new HashSet<>(0);
            HashSet<Author> aP2 = new HashSet<>(0);
            HashSet<Author> aP3 = new HashSet<>(0);
            aP1.add(daniel);
            aP2.add(daniel); aP2.add(jose);
            aP3.add(jose); aP3.add(rodolfo);

            HashSet<Paper> pP1 = new HashSet<>(0);
            HashSet<Paper> pP2 = new HashSet<>(0);
            HashSet<Paper> pP3 = new HashSet<>(0);

            Paper paper1 = new Paper("paper1", aP1, dP1, "FEUP", pP1);

            pP2.add(paper1);
            Paper paper2 = new Paper("paper2", aP2, dP2, "FEUP", pP2);

            pP3.add(paper1); pP3.add(paper2);
            Paper paper3 = new Paper("paper3", aP3, dP3, "FEUP", pP3);


            vdm_System vdmSystem = new vdm_System();
            vdmSystem.addAuthor(daniel); vdmSystem.addAuthor(jose); vdmSystem.addAuthor(rodolfo);
            vdmSystem.addPaper(paper1);
            vdmSystem.addPaper(paper2);
            vdmSystem.addPaper(paper3);

            System.out.println(vdmSystem.coAuthorDistance(daniel, rodolfo));

            System.out.println("Cenas");

        } catch (CGException e) {
            e.printStackTrace();
        }
    }
}
